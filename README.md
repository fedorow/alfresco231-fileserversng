# Alfresco 23.1 + fileServerNG

This is test repository for implementeting SMB on Alfresco 23.1 repository.
It was created by:
- Alfresco Docker Installer https://github.com/Alfresco/alfresco-docker-installer
- fileServerNG module version 62-1.0.18	from 2023-12-15 https://www.filesys.org/kits/fileserversng/alfresco-7.x/

> The project does not work.
> There is an issue to start module.

## How to use
Prerequests: Installed Docker and Docker compose

1. Clone reopsitory
```bash
git clone https://gitlab.com/fedorow/alfresco231-fileserversng
cd alfresco231-fileserversng
```
2. Create volume directories
```bash
sudo ./create_volumes.sh
```
3. Build and start project
```bash
docker compose up -d --build --force-recreate
```
4. Check the logs
```bash
docker comopse logs -f
```
5. Stop the project
```bash
docker compose down
```
